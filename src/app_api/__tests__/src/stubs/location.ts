export const GoodLocation = {
    address1: "Buckingham Palace",
    address2: "London SW1A 1AA, Great Britain",
    city: "London",
    lat: 51.50142361629932,
    lng: -0.14186988249503604,
    postcode: "SW1A 1AA"
}

export const WrongLocation = {
    address1: "Out of boundary",
    address2: "-",
    city: "London",
    lat: 151.50142361629932,
    lng: -110.14186988249503604,
    postcode: "-"
}