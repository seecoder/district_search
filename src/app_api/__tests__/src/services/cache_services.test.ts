import CacheService from "../../../src/services/cache_service";
import { CacheStubValue } from "../stubs/cache";

const originalEnv = process.env;
const cacheKey = "CACHE_KEY";

beforeEach(() => {
    jest.resetModules();
    process.env = {
      ...originalEnv,
      REDIS_URL: "redis",
    };
});

describe('Cache Service', () => {
    test('It should cache value...', async () => {
        const cacheService = new CacheService();

        await cacheService.set(cacheKey, CacheStubValue);
        
        const result = await cacheService.get(cacheKey);
        
        await cacheService.disconnect();

        expect(result).toEqual(CacheStubValue);
    });

    test('It should get value from cache...', async () => {
        const cacheService = new CacheService();        
        const result = await cacheService.get(cacheKey);
        
        await cacheService.disconnect();

        expect(result).toEqual(CacheStubValue);
    });
});

afterEach(async () => {
    process.env = originalEnv;
});