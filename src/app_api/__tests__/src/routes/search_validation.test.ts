import { FindAddressParamsSchemaValidator } from '../../../src/routes/search_validation';

describe('Search By Address Validation', () => {
    test('It should pass if params are ok...', async () => {
        const params = {
            address: "iamstring",
        };
        
        const result = await FindAddressParamsSchemaValidator(params);

        expect(result).toEqual(params);
    });

    test('It should throw error if params are empty...', async () => {
        const params = {
        };
        const stubbedError = async () => await FindAddressParamsSchemaValidator(params);
    
        await expect(stubbedError()).rejects.toThrow();
    });
});