
import DistrictRepository from '../../../src/repositories/district_repository';
import districts from '../stubs/formatted-districts.json';
import { GoodLocation, WrongLocation } from '../stubs/location';


describe('District repository', () => {
    test('It should find location if in boundaries...', async () => {        
        const districtRepository = new DistrictRepository(districts);
        const district = await districtRepository.findByLocation(GoodLocation);
        const expectedResult = {
            serviceArea: "LONCENTRAL"
        };

        expect(district).toEqual(expectedResult);
    });

    test('It should not find location if out of boundaries...', async () => {        
        const districtRepository = new DistrictRepository(districts);
        const district = await districtRepository.findByLocation(WrongLocation);
        const expectedResult = null;

        expect(district).toEqual(expectedResult);
    });
});