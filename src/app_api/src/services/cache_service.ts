
import { createClient } from 'redis';
import { ICacheService } from '../interfaces/cache_interface';

export default class CacheService implements ICacheService {
    private connection = null;

    constructor() {
        try {
            this.connection = createClient({
                url: `redis://${process.env.REDIS_URL}`
            });
        } catch (e) {
            throw e;
        }
        
    }

    private connect = async (): Promise<void> => {
        try {
            if (!this.connection.isOpen) {
                await this.connection.connect();
            }
        } catch (error) {
            throw error;
        }
    }

    public disconnect = async (): Promise<void> => {
        try {
            if (this.connection.isOpen) {
                await this.connection.disconnect();
            }
        } catch (error) {
            throw error;
        }
    }

    public get = async (key: string): Promise<any> => {
        try {
            await this.connect();

            let result = await this.connection.get(key);

            if (!result) {
                return null;
            }
    
            return JSON.parse(result)['value'];
        } catch (error) {
            throw error;
        }
    }

    public set = async (key: string, value: any, expireTime: number = 3600): Promise<void> => {
        try {
            await this.connect();
            
            await this.connection.set(key, JSON.stringify({value}), {
                EX: expireTime,
                NX: true
            });
        } catch (error) {
            throw error;
        }
    }

}