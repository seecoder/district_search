import { IDistrictRepository, IDistrictService } from "../interfaces/district_interface";
import DistrictRepository from "../repositories/district_repository";
import { DatabaseType } from "../types/database_type";
import { District } from "../types/district_type";
import { Location } from "../types/location_type";

export default class DistrictService implements IDistrictService {
    private districtRepository: IDistrictRepository = null;

    constructor(database: DatabaseType) {
        this.districtRepository = new DistrictRepository(database);
    }

    public findByLocation = async (location: Location): Promise<District> => {
        try {
            return await this.districtRepository.findByLocation(location);
        } catch (error) {
            throw error;
        }
    }

}