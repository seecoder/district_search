import { ILocationProvider, ILocationProviderService, ILocationService } from "../../interfaces/location_interface";
import { Location } from "../../types/location_type";

export default class LocationProvider implements ILocationProvider {
    private services: ILocationProviderService[] = [];

    public register = (name: string, service: ILocationService): ILocationProviderService[] => {
        const provider = this.services.find(provider => provider.name === name);

        if (!provider) {    
            this.services.push({
                name,
                service
            });
        }

        return this.services;
    }

    public searchLocation = async (address: string): Promise<Location> => {
        try {
            let location = null;
            let i = 0;

            while (!location && i < this.services.length) {
                location = await this.services[i].service.searchLocation(address);
                i++;
            }
    
            return location;
        } catch (error) {
            throw error;
        }
    }
}