import { ILocationProvider, ILocationService } from "../../interfaces/location_interface";
import { Location } from "../../types/location_type";

export default class GoogleLocationService implements ILocationService {
    private serviceName = "googleLocationService";

    constructor(locationProvider: ILocationProvider) {
        locationProvider.register(this.serviceName, this);
    }

    public static serviceName = () => this.serviceName;
    
    public searchLocation = async (address: string): Promise<Location> => {
        //implemented google maps API, stub response
        // Buckingham Palace, => 51.50142361629932, -0.14186988249503604

        return Promise.resolve(address.toLocaleLowerCase() === "buckingham" ? {
            address1: "Buckingham Palace",
            address2: "London SW1A 1AA, Great Britain",
            city: "London",
            lat: 51.50142361629932,
            lng: -0.14186988249503604,
            postcode: "SW1A 1AA"
        } : null);
    }
}