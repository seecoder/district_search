import { ContextType } from "../types/context_type";
import CacheService from "./cache_service";
import DistrictService from "./district_service";
import GoogleLocationService from "./locations/google_location_service";
import LocationProvider from "./locations/location_provider";

export default (app: any, context: ContextType): void => {
    const locationProvider = new LocationProvider();
    new GoogleLocationService(locationProvider);

    const districtService = new DistrictService(context.database);
    const cacheService = new CacheService();

    context.services = {
        locationProvider,
        districtService,
        cacheService
    };
}