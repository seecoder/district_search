import { ContextType } from '../types/context_type';
import SearchRoute from './search_route';

export default (app: any, context: ContextType): void => {
    SearchRoute(app, context);
}