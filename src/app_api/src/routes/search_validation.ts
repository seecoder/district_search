import { object, string, number } from 'yup';

export const FindAddressParamsSchemaValidator = async (params) => {
    const findAddressParamsSchema = object({
        address: string().required(),
    });

    return await findAddressParamsSchema.validate(params)
}