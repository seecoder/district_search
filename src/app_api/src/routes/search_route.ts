import { NextFunction, Response } from "express";
import { ContextType } from "../types/context_type";
import { SearchByAddressRequestType } from "../types/search_request_type";
import { SearchByAddressResponseType } from "../types/search_response_type";
import { FindAddressParamsSchemaValidator } from "./search_validation";

const getCacheKey = (params: any) => {
    let key = "SEACH_BY_ADDRESS_";

    Object.keys(params).map((param: any) => key += `${param}_${params[param]}`);

    return key;
}

export default (app: any, context: ContextType) => {
    app.get('/search/address/:address', async (req: SearchByAddressRequestType, res: Response, next: NextFunction) => {
        try {
            await FindAddressParamsSchemaValidator(req.params);
            const cacheKey = getCacheKey(req.params)
            let data = await context.services.cacheService.get(cacheKey) as SearchByAddressResponseType;

            if (!data) {            
                const location = await context.services.locationProvider.searchLocation(req.params.address);

                if (location) {
                    const district = await context.services.districtService.findByLocation(location);

                    if (district) {
                        data = {
                            status: "OK",
                            search: req.params.address,
                            location: {...location, ...district}
                        };
                    }
                }

                if (!data) {
                    data = {
                        status: "NOT_FOUND",
                        search: "Non-existing address"
                    }
                }

                await context.services.cacheService.set(cacheKey, data);
            }

            res.json(data)
        } catch (error) {
            next(error);
        }
    })
}