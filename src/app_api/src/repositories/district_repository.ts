import { IDistrictRepository } from "../interfaces/district_interface";
import { DatabaseFeatureType, DatabaseType } from "../types/database_type";
import { District } from "../types/district_type";
import { Location } from "../types/location_type";

export default class DistrictRepository implements IDistrictRepository {
    private database: DatabaseType = null;

    constructor(database: DatabaseType) {
        this.database = database;
    }

    private getEdgeValues = (values: any): any => {
        return {
            max: Math.max(...values),
            min: Math.min(...values),
        }
    }

    findByLocation = (location: Location): Promise<District> => {
        //usually I would put this logic into service
        const foundDistrict = this.database.features.find((feature: DatabaseFeatureType) => {
            const coordinates = feature.geometry.coordinates[0].map(coord => {
                const [lng, lat, z] = coord;

                return {
                    lng: lng *(-1),
                    lat,
                    z
                }
            });

            const edgeLng = this.getEdgeValues(coordinates.map(coord => coord.lng));

            if ((location.lng * (-1)) <= edgeLng.min || (location.lng * (-1)) >= edgeLng.max) {
                return null;
            }

            const edgeLat = this.getEdgeValues(coordinates.map(coord => coord.lat));

            if (location.lat < edgeLat.min || location.lat > edgeLat.max) {
                return null;
            }

            return feature;
        });
        
        return Promise.resolve(foundDistrict ? {serviceArea: foundDistrict?.properties?.Name} : null);
    }
}