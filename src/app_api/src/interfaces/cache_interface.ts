export interface ICacheService {
    disconnect: () => Promise<void>;
    get: (key: string) => Promise<any>;
    set: (key: string, value: any, expireTime?: number) => Promise<void>;
}