import { District } from "../types/district_type";
import { Location } from "../types/location_type";

export interface IDistrictService {
    findByLocation: (location: Location) => Promise<District>;
}

export interface IDistrictRepository {
    findByLocation: (location: Location) => Promise<District>;
}