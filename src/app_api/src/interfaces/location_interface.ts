import { Location } from "../types/location_type";

export interface ILocationProvider {
    register: (name: string, service: any) => any;
    searchLocation: (address: string) => Promise<Location>;
}

export interface ILocationService {
    searchLocation: (address: string) => Promise<Location>;
}

export interface ILocationProviderService {
    name: string;
    service: ILocationService;
}