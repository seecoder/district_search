import fs from "fs";

export const jsonToObject = (filename: string) => {
    try {
        const rawData = fs.readFileSync(`${__dirname}/../data/json/${filename}` , 'utf8');
        
        return JSON.parse(rawData);
    } catch (error) {
        throw error;
    }
}