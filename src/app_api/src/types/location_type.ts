export type Location = {
    address1: string;
    address2: string;
    city: string;
    lat: number;
    lng: number;
    postcode: string;
}