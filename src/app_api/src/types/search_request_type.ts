import { Request } from "express";

export type SearchByAddressRequestType = Request & {
    params: {
        address: string;
    }
}