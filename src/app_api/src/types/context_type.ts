import { ICacheService } from "../interfaces/cache_interface";
import { IDistrictService } from "../interfaces/district_interface";
import { ILocationProvider } from "../interfaces/location_interface";
import { DatabaseType } from "./database_type";

export type ContextType = {
    database: DatabaseType,
    services: {
        locationProvider: ILocationProvider,
        districtService: IDistrictService,
        cacheService: ICacheService
    }
};