import { District } from "./district_type";
import { Location } from "./location_type";

export type SearchByAddressResponseType = {
    status: string;
    search: string;
    location?: Location & District
};