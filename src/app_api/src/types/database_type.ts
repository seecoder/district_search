export type DatabaseType = {
    crs: {
        properties: {
            name: string;
        },
        type: string;
    },
    features: DatabaseFeatureType[]
}

export type DatabaseFeatureType = {
    type: string;
    geometry: {
        type: string;
        coordinates: number[][][]
    },
    properties: {
        Description: string;
        Name: string;
    }

}