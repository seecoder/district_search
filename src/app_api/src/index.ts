import RouteResolver from "./routes/index";
import { jsonToObject } from "./loaders/data_loader";
import ServiceResolver from "./services/index";
import express from 'express';
import { ContextType } from "./types/context_type";
  
const app = express()
const port = process.env.PORT || 7015;
const database = jsonToObject('formatted-districts.json');
const context: ContextType = {
    database,
    services: null,
};

ServiceResolver(app, context);
RouteResolver(app, context);

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})


