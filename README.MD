About the project:

Application resolves a given human-readable address into a district. The list of districts is provided as a separate file
data/json/formated-districts.json . The response is a structure that contains latitude, longitude, service area and postcode at least.

{
    “status”: ”OK”,
    “search”: “White Bear Yard”,
    “location”: {
        “address1”: “2nd Floor, White Bear Yard”,
        “address2”: “144a Clerkenwell Road”,
        “city”: “London”,
        “lat”: 0.0000,
        “lng”: 0.0000,
        “serviceArea”: “LONCENTRAL”
        “postcode”: “EC1R5DF”
    }
}

How to run the project:

1. ./build
2. ./run
3. ./ssh

Now you should be inside docker environment

4. cd app_api
5. yarn install
6. cd ..
7. pm2 start process.json
8. pm2 log
9. Go to browser
10. Open localhost:7015/search/address/buckinhgam


How to run tests:

1. ./ssh
2. cd app_api
3. yarn test


Notes:

Location search algorithm works fine for "buckingham" key word. For any other it will return
{
    “status”: “NOT_FOUND”,
    “search”: “Non-existing address”
}